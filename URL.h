#pragma once
#include <string>

/*
Class: Url

Object which holds the url information such as the actual URL and the visit time
*/
class Url
{
public:

	static std::string encode(const std::string& str);
	static std::string decode(const std::string& str);

#ifndef __linux__
	static std::wstring encode(const std::wstring& str);
	static std::wstring decode(const std::wstring& str);
#endif
};

std::wstring AnsiToUnicode(const std::string& src);